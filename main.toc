\babel@toc {brazil}{}
\contentsline {section}{\numberline {1}\sc Relatividade, Formula\IeC {\c c}\IeC {\~a}o Covariante}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Relatividade restrita}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Simultaneidade, efeitos relativ\IeC {\'\i }sticos}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Grupo de Lorentz}{5}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Grupo de Poincar\IeC {\'e}}{7}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}$\ttau $, 4-vetores}{9}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Adi\IeC {\c c}\IeC {\~a}o de velocidades}{11}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Equa\IeC {\c c}\IeC {\~o}es de Maxwell Covariante}{13}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Equa\IeC {\c c}\IeC {\~o}es de Maxwell na forma Integral}{15}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Transforma\IeC {\c c}\IeC {\~o}es de Campos, Invariantes}{17}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Campo part\IeC {\'\i }cula em movimento}{19}{subsection.1.10}
\contentsline {section}{\numberline {2}\sc Eletrodin\IeC {\^a}mica, Radia\IeC {\c c}\IeC {\~a}o}{21}{section.2}
\contentsline {subsection}{\numberline {2.1}Lagrangeana, Equa\IeC {\c c}\IeC {\~o}es de Maxwell via Euler-Lagrange}{21}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Equa\IeC {\c c}\IeC {\~a}o de Movimento para part\IeC {\'\i }cula}{23}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Hamiltoniana para part\IeC {\'\i }cula no Campo}{25}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Simetrias e Leis de conserva\IeC {\c c}\IeC {\~a}o}{27}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Tensor $\tT [^\mu ^\nu ]$}{29}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Conserva\IeC {\c c}\IeC {\~a}o de momentos}{31}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Solu\IeC {\c c}\IeC {\~o}es fundamentais de $\tpartial ^2$}{33}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Potenciais retardados}{35}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Campo retardado para part\IeC {\'\i }cula acelerada}{37}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Distribui\IeC {\c c}\IeC {\~a}o angular de pot\IeC {\^e}ncia}{39}{subsection.2.10}
\contentsline {section}{\numberline {3}\sc Solu\IeC {\c c}\IeC {\~o}es est\IeC {\'a}ticas, Meios cont\IeC {\'\i }nuos, Ondas}{41}{section.3}
\contentsline {subsection}{\numberline {3.1}Solu\IeC {\c c}\IeC {\~o}es fundamentais de $\nabla ^2$}{41}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Solu\IeC {\c c}\IeC {\~a}o formal da equa\IeC {\c c}\IeC {\~a}o de Poisson}{43}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Problema de Dirichlet e Neumann}{45}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Separa\IeC {\c c}\IeC {\~a}o de Vari\IeC {\'a}veis}{47}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Expans\IeC {\~a}o multipolar - Cartesianas}{49}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Expans\IeC {\~a}o multipolar - Esf\IeC {\'e}ricas}{51}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Equa\IeC {\c c}\IeC {\~o}es de Maxwell em meios cont\IeC {\'\i }nuos, $P$ $D$}{53}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Condi\IeC {\c c}\IeC {\~o}es de contorno de interface}{55}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Ondas EM, par\IeC {\^a}metros de Stokes}{57}{subsection.3.9}
